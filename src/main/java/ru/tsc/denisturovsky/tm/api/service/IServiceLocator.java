package ru.tsc.denisturovsky.tm.api.service;

public interface IServiceLocator {

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

}
