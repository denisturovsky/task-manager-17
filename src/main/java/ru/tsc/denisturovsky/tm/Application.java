package ru.tsc.denisturovsky.tm;

import ru.tsc.denisturovsky.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
